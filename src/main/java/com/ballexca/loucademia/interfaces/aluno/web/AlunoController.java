package com.ballexca.loucademia.interfaces.aluno.web;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.ballexca.loucademia.application.service.AlunoService;
import com.ballexca.loucademia.application.util.StringUtils;
import com.ballexca.loucademia.domain.aluno.Aluno;
import com.ballexca.loucademia.interfaces.shared.web.Router;

@Named
@RequestScoped
public class AlunoController implements Serializable {
  
  @EJB
  private AlunoService alunoService;
  
  @Inject
  private FacesContext facesContext;
  
  private Aluno aluno = new Aluno();
  
  private String matricula;
  
  private String titulo = "Novo Aluno";
  
  public void carregar() {
    if(!StringUtils.estaVazia(matricula)) {
      aluno = alunoService.findByMatricula(matricula);
      titulo = "Alterar Aluno";
    }
  }
  
  public Object gravar() {
    alunoService.createOrUpdate(aluno);
    facesContext.addMessage(null, new FacesMessage("Dados gravados com sucesso!"));
    
    return Router.mesmaPagina;
  }
 
  public Aluno getAluno() {
    return aluno;
  }

  public void setAluno(Aluno aluno) {
    this.aluno = aluno;
  }
  
  public void setMatricula(String matricula) {
    this.matricula = matricula;
  }
  
  public String getMatricula() {
    return matricula;
  }
  
  public String getTitulo() {
    return titulo;
  }
  
  public boolean isRenderizaPainelMensagem() {
    return !facesContext.getMessageList().isEmpty();
  }
}
