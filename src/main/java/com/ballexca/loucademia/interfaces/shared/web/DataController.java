package com.ballexca.loucademia.interfaces.shared.web;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.ballexca.loucademia.application.service.DataService;
import com.ballexca.loucademia.domain.aluno.Aluno.Sexo;
import com.ballexca.loucademia.domain.aluno.Aluno.Situacao;
import com.ballexca.loucademia.domain.aluno.Estado;

@Named
@ApplicationScoped
public class DataController implements Serializable {
  
  @EJB
  private DataService dataService;
  
  public List<Sexo> getSexos() {
    return dataService.getSexos();
  }
  
  public List<Situacao> getSituacoes() {
    return dataService.getSituacoes();
  }

  public List<Estado> getEstados() {
    return dataService.listEstados();
  }

}
