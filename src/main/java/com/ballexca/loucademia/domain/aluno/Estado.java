package com.ballexca.loucademia.domain.aluno;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ESTADO")
public class Estado implements Serializable {

  @Id
  @Column(name = "SIGLA", nullable = false, length = 2)
  private String sigla;
 
  @Column(name = "NOME", nullable = false, length = 30)
  private String nome;
  
  public String getNome() {
    return nome;
  }
  public void setNome(String nome) {
    this.nome = nome;
  }
  public String getSigla() {
    return sigla;
  }
  public void setSigla(String sigla) {
    this.sigla = sigla;
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Estado [getNome()=");
    builder.append(getNome());
    builder.append(", getSigla()=");
    builder.append(getSigla());
    builder.append("]");
    
    return builder.toString();
  }
  @Override
  public int hashCode() {
    return Objects.hash(sigla);
  }
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Estado other = (Estado) obj;
    return Objects.equals(sigla, other.sigla);
  }
  
}
