package com.ballexca.loucademia.domain.aluno;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Year;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ballexca.loucademia.application.util.StringUtils;

@Entity
@Table(name = "ALUNO")
public class Aluno implements Serializable {

  public enum Sexo {
    Masculino, Feminino;
  }

  public enum Situacao {
    Ativo, Inativo, Pendente;
  }

  @Id
  @Column(name = "ID", nullable = false, length = 8)
  private String matricula;

  @Column(name = "NOME", nullable = false, length = 64)
  private String nome;

  @Enumerated
  @Column(name = "SEXO", nullable = false)
  private Sexo sexo;

  @Column(name = "RG", nullable = false, length = 10)
  private Integer rg;

  @Column(name = "NASCIMENTO", nullable = false)
  private LocalDate dataNascimento;

  @Enumerated
  @Column(name = "SITUACAO", nullable = false)
  private Situacao situacao;

  @Column(name = "EMAIL", nullable = false, length = 64)
  private String email;

  @Embedded
  private Endereco endereco = new Endereco();

  @Embedded
  private Telefone telefone = new Telefone();

  public void gerarMatricula(String maxMatricula) {
    Year ano = Year.now();
    
    if (StringUtils.estaVazia(maxMatricula)) {
      maxMatricula = ano + StringUtils.adicionaZeroAEsquerda(0,4);
    }
    
    int sequencial =  Integer.parseInt(maxMatricula.substring(4));
    sequencial++;
    
    this.matricula = ano + StringUtils.adicionaZeroAEsquerda(sequencial, 4);
  }

  public String getMatricula() {
    return matricula;
  }

  public void setMatricula(String matricula) {
    this.matricula = matricula;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public Sexo getSexo() {
    return sexo;
  }

  public void setSexo(Sexo sexo) {
    this.sexo = sexo;
  }

  public Integer getRg() {
    return rg;
  }

  public void setRg(Integer rg) {
    this.rg = rg;
  }

  public LocalDate getDataNascimento() {
    return dataNascimento;
  }

  public void setDataNascimento(LocalDate dataNascimento) {
    this.dataNascimento = dataNascimento;
  }

  public Situacao getSituacao() {
    return situacao;
  }

  public void setSituacao(Situacao situacao) {
    this.situacao = situacao;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Endereco getEndereco() {
    return endereco;
  }

  public void setEndereco(Endereco endereco) {
    this.endereco = endereco;
  }

  public Telefone getTelefone() {
    return telefone;
  }

  public void setTelefone(Telefone telefone) {
    this.telefone = telefone;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Aluno [getMatricula()=");
    builder.append(getMatricula());
    builder.append(", getNome()=");
    builder.append(getNome());
    builder.append(", getSexo()=");
    builder.append(getSexo());
    builder.append(", getRg()=");
    builder.append(getRg());
    builder.append(", getDataNascimento()=");
    builder.append(getDataNascimento());
    builder.append(", getSituacao()=");
    builder.append(getSituacao());
    builder.append(", getEmail()=");
    builder.append(getEmail());
    builder.append(", getEndereco()=");
    builder.append(getEndereco());
    builder.append(", getTelefone()=");
    builder.append(getTelefone());
    builder.append("]");

    return builder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(matricula);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Aluno other = (Aluno) obj;
    return Objects.equals(matricula, other.matricula);
  }
}
