package com.ballexca.loucademia.domain.aluno;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class EstadoRepository {

  @PersistenceContext
  private EntityManager manager;

  public List<Estado> listEstados() {
    TypedQuery<Estado> queryEstados = manager.createQuery("SELECT e FROM Estado e ORDER BY e.nome", Estado.class);
    return queryEstados.getResultList();
  }
}
