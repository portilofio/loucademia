package com.ballexca.loucademia.domain.aluno;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Telefone implements Serializable {

  @Column(name = "CELULAR_DDD", nullable = false, length = 2)
  private String dddCelular;
  
  @Column(name = "CELULAR_NUMERO", nullable = false, length = 9)
  private String numeroCelular;
 
  @Column(name = "FIXO_DDD", nullable = true, length = 2)
  private String dddFixo;
  
  @Column(name = "FIXO_NUMERO", nullable = true, length = 9)
  private String numeroFixo;
  
  public String getDddCelular() {
    return dddCelular;
  }
  public void setDddCelular(String dddCelular) {
    this.dddCelular = dddCelular;
  }
  public String getNumeroCelular() {
    return numeroCelular;
  }
  public void setNumeroCelular(String numeroCelular) {
    this.numeroCelular = numeroCelular;
  }
  public String getDddFixo() {
    return dddFixo;
  }
  public void setDddFixo(String dddFixo) {
    this.dddFixo = dddFixo;
  }
  public String getNumeroFixo() {
    return numeroFixo;
  }
  public void setNumeroFixo(String numeroFixo) {
    this.numeroFixo = numeroFixo;
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Telefone [getDddCelular()=");
    builder.append(getDddCelular());
    builder.append(", getNumeroCelular()=");
    builder.append(getNumeroCelular());
    builder.append(", getDddFixo()=");
    builder.append(getDddFixo());
    builder.append(", getNumeroFixo()=");
    builder.append(getNumeroFixo());
    builder.append("]");
    
    return builder.toString();
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(dddCelular, dddFixo, numeroCelular, numeroFixo);
  }
  
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Telefone other = (Telefone) obj;
    return Objects.equals(dddCelular, other.dddCelular) && Objects.equals(dddFixo, other.dddFixo)
        && Objects.equals(numeroCelular, other.numeroCelular) && Objects.equals(numeroFixo, other.numeroFixo);
  }
  
}
