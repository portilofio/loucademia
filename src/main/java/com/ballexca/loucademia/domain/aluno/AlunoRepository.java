package com.ballexca.loucademia.domain.aluno;

import java.time.Year;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class AlunoRepository {

  @PersistenceContext
  private EntityManager manager;

  public void store(Aluno aluno) {
    manager.persist(aluno);
  }

  public Aluno update(Aluno aluno) {
    return manager.merge(aluno);
  }

  public Aluno findByMatricula(String matricula) {
    return manager.find(Aluno.class, matricula);
  }

  public String getMaxMatriculaAno() {
    return manager
        .createQuery("SELECT MAX(a.matricula) FROM Aluno a WHERE a.matricula LIKE :ano", String.class)
        .setParameter("ano",Year.now() + "%")
        .getSingleResult();
  }
  
  public void delete(String matricula) {
    Aluno aluno = findByMatricula(matricula);

    if (aluno != null) {
      manager.remove(aluno);
    }
  }
}
