package com.ballexca.loucademia.domain.acesso;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ballexca.loucademia.domain.aluno.Aluno;

@Entity
@Table(name = "ENTRAS_SAIDAS")
public class Acesso implements Serializable {

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  
  @ManyToOne
  @JoinColumn(name = "ALUNO_ID", nullable = false)
  private Aluno aluno;

  @Column(name = "ENTRADA", nullable = false)
  private LocalDateTime entrada;
  
  @Column(name = "SAIDA", nullable = true)
  private LocalDateTime saida;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Aluno getAluno() {
    return aluno;
  }

  public void setAluno(Aluno aluno) {
    this.aluno = aluno;
  }

  public LocalDateTime getEntrada() {
    return entrada;
  }

  public void setEntrada(LocalDateTime entrada) {
    this.entrada = entrada;
  }

  public LocalDateTime getSaida() {
    return saida;
  }

  public void setSaida(LocalDateTime saida) {
    this.saida = saida;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Acesso [getId()=");
    builder.append(getId());
    builder.append(", getAluno()=");
    builder.append(getAluno());
    builder.append(", getEntrada()=");
    builder.append(getEntrada());
    builder.append(", getSaida()=");
    builder.append(getSaida());
    builder.append("]");
    
    return builder.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Acesso other = (Acesso) obj;
    return Objects.equals(id, other.id);
  }
  
}
