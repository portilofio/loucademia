package com.ballexca.loucademia.application.util;

public class Validation {

  public static void assertNotEmpty(Object objeto) {
    if(objeto instanceof String) {
      String s = (String) objeto;
      if(StringUtils.estaVazia(s)) {
        throw new ValidationException("O texto não pode ser nulo.");
      }
    }
    
    if(objeto == null) {
      throw new ValidationException("Valor não pode ser nulo.");
    }
  }
}
