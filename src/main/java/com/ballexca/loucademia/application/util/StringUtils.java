package com.ballexca.loucademia.application.util;

public class StringUtils {

  public static boolean estaVazia(String s) {
    if(s == null) {
      return true;
    }
  
    return s.trim().length() == 0;
  }

  public static String adicionaZeroAEsquerda(int valor, int tamanhoFinal) {
    return String.format("%0" + tamanhoFinal + "d", valor);
  }
}
