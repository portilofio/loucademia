package com.ballexca.loucademia.application.service;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.ballexca.loucademia.domain.aluno.Estado;
import com.ballexca.loucademia.domain.aluno.EstadoRepository;
import com.ballexca.loucademia.domain.aluno.Aluno.Sexo;
import com.ballexca.loucademia.domain.aluno.Aluno.Situacao;

@Stateless
public class DataService {

  @EJB
  private EstadoRepository estadoRepository;

  public List<Estado> listEstados() {
    return estadoRepository.listEstados();
  }

  public List<Sexo> getSexos() {
    return Arrays.asList(Sexo.values());
  }
  
  public List<Situacao> getSituacoes() {
    return Arrays.asList(Situacao.values());
  }
}
