package com.ballexca.loucademia.application.service;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.ballexca.loucademia.application.util.StringUtils;
import com.ballexca.loucademia.application.util.Validation;
import com.ballexca.loucademia.domain.aluno.Aluno;
import com.ballexca.loucademia.domain.aluno.AlunoRepository;

@Stateless
public class AlunoService {

  @EJB
  private AlunoRepository alunoRepository;

  public void createOrUpdate(Aluno aluno) {
    if (StringUtils.estaVazia(aluno.getMatricula())) {
      create(aluno);
    } else {
      update(aluno);
    }
  }

  private void create(Aluno aluno) {
    Validation.assertNotEmpty(aluno);
    String maxMatriculaAno = alunoRepository.getMaxMatriculaAno();
    aluno.gerarMatricula(maxMatriculaAno);
    alunoRepository.store(aluno);
  }

  private void update(Aluno aluno) {
    Validation.assertNotEmpty(aluno);
    alunoRepository.update(aluno);
  }

  public Aluno findByMatricula(String matricula) {
    Validation.assertNotEmpty(matricula);
    return alunoRepository.findByMatricula(matricula);
  }
}
